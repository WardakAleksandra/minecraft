Instrukcja uruchomienia:

1. Zainstalować bibliotekę Irrlicht przy pomocy skryptu:
	./install_lib.sh
2. Wykonać makefile:
	cd source/src/
	make
3. Uruchomić aplikację w folderze bin:
	cd ../../bin/
	./Minecraft
4. Czerpać przyjemność z gry :)
