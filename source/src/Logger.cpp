#include "Logger.h"

Logger& Logger::getInstance()
{
  static Logger instance;
  return instance;
}

Logger::Logger(){}

void Logger::d(std::string TAG, std::string msg)
{
  std::cout<<"DEBUG: "<<TAG<<": "<<msg<<std::endl;
}

void Logger::e(std::string TAG, std::string msg)
{
  std::cout<<"ERROR: "<<TAG<<": "<<msg<<std::endl;
}

void Logger::i(std::string TAG, std::string msg)
{
  std::cout<<"INFO: "<<TAG<<": "<<msg<<std::endl;
}

 void Logger::w(std::string TAG, std::string msg)
{
  std::cout<<"WARNING: "<<TAG<<": "<<msg<<std::endl;
}
