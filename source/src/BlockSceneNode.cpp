/*
Author:
    Wardak Aleksandra

Class BlockSceneNode describes signle block used to create game world.
*/

#include "BlockSceneNode.h"
#include <irrlicht.h>

BlockSceneNode::BlockSceneNode(scene::ISceneNode* parent, scene::ISceneManager* mgr, s32 id, BlockModel* block)
  : scene::ISceneNode(parent, mgr, id)
  {
    material.Wireframe = false;
    material.Lighting = false;
    float length = static_cast<float> (block->getBlockSize()) / 2;
    core::vector3df centrum = block->getCentrum();
    vertices = block->getVertices();
   // number_of_vertices = sizeof(vertices) / sizeof(vertices[0]);
    number_of_vertices = block->getNumberOfVertices();
    box.reset(vertices[0].Pos);
    for (s32 i = 1; i < number_of_vertices; ++i)
    {
        box.addInternalPoint(vertices[i].Pos);
    }
  }

BlockSceneNode::~BlockSceneNode()
{

}

void BlockSceneNode::OnRegisterSceneNode()
{
    if (IsVisible)
        SceneManager->registerNodeForRendering(this);

    ISceneNode::OnRegisterSceneNode();
}

void BlockSceneNode::render()
{
    u16 indices[] = {	  0, 1, 5, 4,
                        1, 2, 6, 5,
                        2, 3, 7, 6,
                        3, 0, 4, 7,
                        0, 1, 2, 3,
                        4, 5, 6, 7
                        };
    video::IVideoDriver* driver = SceneManager->getVideoDriver();

    driver->setMaterial(material);
    driver->setTransform(video::ETS_WORLD, AbsoluteTransformation);
    driver->drawVertexPrimitiveList(&vertices[0], number_of_vertices, &indices[0], NUMBER_OF_SIDES,
                video::EVT_STANDARD, scene::EPT_QUADS, video::EIT_16BIT);
}

const core::aabbox3d<f32>& BlockSceneNode::getBoundingBox() const
{
    return box;
}

u32 BlockSceneNode::getMaterialCount() const
{
    return 1;
}

video::SMaterial& BlockSceneNode::getMaterial(u32 i)
{
    return material;
}
