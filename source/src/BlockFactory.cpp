#include "BlockFactory.h"
#include "BlockGround.h"
#include "BlockRock.h"

BlockFactory::BlockFactory()
{
    //ctor
}

BlockFactory::~BlockFactory()
{
    //dtor
}

BlockModel* BlockFactory::createBlock(const Type type_, core::vector3df centrum)
{
  switch(type_)
  {
  case Type::GROUND:
    return new BlockGround(centrum);
    break;
  case Type::ROCK:
    return new BlockRock(centrum);
    break;
  default:
    return new BlockGround(centrum);
    break;
  }

}
