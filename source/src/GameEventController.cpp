#include "GameEventController.h"
#include "SceneDrawManager.h"
#include "GameParams.h"
#include "Logger.h"
#include "BlockFactory.h"
#include "MapController.h"
#include <boost/math/special_functions/sign.hpp>
#include <irrlicht.h>

Logger& Log = Logger::getInstance();
const std::string TAG = "GameEventController";

GameEventController::GameEventController()
{
  //ctor
}

GameEventController::~GameEventController()
{
  //dtor
}


int GameEventController::process(GameEventReceiver& receiver, irr::scene::ISceneManager* smgr,
                                 irr::scene::ICameraSceneNode* camera)
{
  MapController& map_controller = MapController::getInstance();
  //----- Keyboard--------------------------------
  if (receiver.IsKeyDown(irr::KEY_ESCAPE))
  {
    return -1;
  }

  if (receiver.IsKeyDown(irr::KEY_KEY_1))
  {
    GameParams::current_type = Type::GROUND;
    //TODO pokazac ryzunek na ekranie
  }

  if (receiver.IsKeyDown(irr::KEY_KEY_2))
  {
    GameParams::current_type = Type::ROCK;
    //TODO pokazac ryzunek na ekranie
  }


  //----- Mouse --------------------------------
  if (receiver.GetMouseState().LeftButtonDown)
    {
      irr::core::line3df ray = smgr->getSceneCollisionManager()->getRayFromScreenCoordinates(
              receiver.GetMouseState().Position, camera);

    // And intersect the ray with a plane around the node facing towards the camera.
    irr::core::vector3df block_position = camera->getPosition();
    irr::core::vector3df cam_direction = camera->getTarget() - camera->getPosition();
    irr::core::vector3df block_versor = cam_direction.normalize();
    block_position.X += block_versor.X * GameParams::BLOCK_SIZE;
    block_position.Y += block_versor.Y * GameParams::BLOCK_SIZE;
    block_position.Z += block_versor.Z * GameParams::BLOCK_SIZE;


    irr::core::plane3df plane(block_position, block_versor);
    irr::core::vector3df mousePosition;

    if(plane.getIntersectionWithLine(ray.start, ray.getVector(), mousePosition))
    {

//    log.d(TAG, "Mouse pressed");
      irr::core::vector3di block_idx = map_controller.coordsToIndexes(block_position);
      Log.d(TAG, "X: " + std::to_string(block_idx.X)+ "Y: " + std::to_string(block_idx.Y)+ "Z: " + std::to_string(block_idx.Z));
      if (map_controller.isAreaAvailable(block_idx))
      {
        try
        {
        map_controller.addBlock(block_idx, GameParams::current_type);
        }
        catch (std::exception exc)
        {
          Log.e(TAG, exc.what());
        }
      }
      else
      {
        //TODO sprawdzic burability bloku i ewentualnie zniszczyc
        map_controller.removeBlock(block_idx);
      }
      receiver.clearMouseState(true);
    }
  }
  return 0;
}

