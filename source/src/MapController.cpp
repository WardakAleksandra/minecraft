#include "MapController.h"
#include "SceneDrawManager.h"
#include "BlockFactory.h"
#include <algorithm>

MapController& MapController::getInstance()
{
  static MapController instance;
  return instance;
}

MapController::MapController()
{
  //ctor
}


void MapController::drawDefaultMap()
{
  map_model.createDefaultMap();
  SceneDrawManager::getInstance().drawMap(map_model);
}

irr::core::vector3df MapController::indexesToCoords(irr::core::vector3di const indexes) const
{
  float x = indexes.X * GameParams::BLOCK_SIZE;
  float y = indexes.Y * GameParams::BLOCK_SIZE;
  float z = indexes.Z * GameParams::BLOCK_SIZE;
  return irr::core::vector3df(x, y, z);
}

irr::core::vector3di MapController::coordsToIndexes(irr::core::vector3df const position) const
{
  int x = round(position.X / GameParams::BLOCK_SIZE);
  int y = round(position.Y / GameParams::BLOCK_SIZE);
  int z = round(position.Z / GameParams::BLOCK_SIZE);
  return irr::core::vector3di(x, y, z);
}

bool MapController::isAreaAvailable(irr::core::vector3di const indexes) const
{
  auto key = std::make_tuple(indexes.X, indexes.Y, indexes.Z);
  auto game_map = map_model.getGameMap();
  Log.d(TAG, "count: " + std::to_string(game_map.count(key)));
  return (game_map.count(key) == 0);
}

void MapController::removeBlock(irr::core::vector3di const indexes)
{
  std::map<std::tuple<int,int,int>, Type> game_map = map_model.getGameMap();
  SceneDrawManager::getInstance().removeBlock(indexes);
  auto key = std::make_tuple(indexes.X, indexes.Y, indexes.Z);
  game_map.erase(key);
  map_model.setGameMap(game_map);
}

void MapController::addBlock(irr::core::vector3di const indexes, Type type)
{
  std::map<std::tuple<int,int,int>, Type> game_map = map_model.getGameMap();
  /*
  Log.d(TAG, "MAP ELEMENTS: ");
  std::for_each(game_map.begin(), game_map.end(), [this](decltype(*begin(game_map)) &map_element){
               int x = std::get<0>(map_element.first);
               int y = std::get<1>(map_element.first);
               int z = std::get<2>(map_element.first);
               Log.d(TAG, std::to_string(x) +" " + std::to_string(y) +" "+ std::to_string(z));

                 });
*/
   SceneDrawManager::getInstance().drawBlock(indexes, type);
   auto key = std::make_tuple(indexes.X, indexes.Y, indexes.Z);
   //Log.d(TAG, "INDEXES: X: " + std::to_string(indexes.X)  + " Y: " + std::to_string(indexes.Y) + " Z: " + std::to_string(indexes.Z));
   //Log.d(TAG, "KEY: X: " + std::to_string(std::get<0>(key)  )+ " Y: " + std::to_string(std::get<1>(key))  + " Z: " + std::to_string(std::get<2>(key)));
   game_map.insert(std::make_pair(key, type));
   map_model.setGameMap(game_map);
}

int MapController::createBlockId(irr::core::vector3di const indexes)
{
   // id created that way is unique in all map, moreover, freeing area in map causes freeing index
  return indexes.X * GameParams::MAP_SIZE * GameParams::MAP_SIZE + indexes.Y * GameParams::MAP_SIZE + indexes.Z;
}
