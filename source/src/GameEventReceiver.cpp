#include "GameEventReceiver.h"

using namespace irr;

GameEventReceiver::GameEventReceiver()
{
  for (unsigned i=0; i<KEY_KEY_CODES_COUNT; ++i)
            keyIsDown[i] = false;
}

GameEventReceiver::~GameEventReceiver()
{
  //dtor
}

bool GameEventReceiver::OnEvent(const SEvent& event)
{
  // Remember whether each key is down or up
  if (event.EventType == irr::EET_KEY_INPUT_EVENT)
  {
     keyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
  } else if(event.EventType == irr::EET_MOUSE_INPUT_EVENT)
  {
//    log.d(TAG, "Mouse event received");
    switch(event.MouseInput.Event)
  {
    case EMIE_LMOUSE_PRESSED_DOWN:
      MouseState.LeftButtonDown = true;
      break;

    case EMIE_LMOUSE_LEFT_UP:
      MouseState.LeftButtonDown = false;
      break;

    case EMIE_MOUSE_MOVED:
      MouseState.Position.X = event.MouseInput.X;
      MouseState.Position.Y = event.MouseInput.Y;
      break;

    default:
      break;
  }
}

return false;
}

 // This is used to check whether a key is being held down
bool GameEventReceiver::IsKeyDown(EKEY_CODE keyCode) const
{
  return keyIsDown[keyCode];
}

const GameEventReceiver::SMouseState& GameEventReceiver::GetMouseState(void) const
{
    return MouseState;
}

void GameEventReceiver::clearMouseState(bool is_state_processed)
{
  MouseState.LeftButtonDown = !is_state_processed;
}
