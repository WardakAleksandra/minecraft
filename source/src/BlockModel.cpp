#include <iostream>
#include "BlockModel.h"
#include "Logger.h"
#define  TAG   "BlockModel"


BlockModel::BlockModel(core::vector3df block_centrum, const video::SColor& color, unsigned block_size) :
    centrum(block_centrum), block_size(block_size)
{
  float length = static_cast<float> (block_size) / 2;

    vertices[0] = video::S3DVertex(centrum.X - length, centrum.Y -length, centrum.Z + length,
                                  0, 0, 0,
                                  color, 0, 0);
    vertices[1] = video::S3DVertex(centrum.X + length, centrum.Y -length, centrum.Z + length,
                                  0,0,0,
                                  color, 0, 0);
    vertices[2] = video::S3DVertex(centrum.X + length, centrum.Y -length, centrum.Z -length,
                                  0,0,0,
                                  color, 0, 0);
    vertices[3] = video::S3DVertex(centrum.X - length, centrum.Y -length, centrum.Z -length,
                                  0,0,0,
                                  color, 0, 0);
    vertices[4] = video::S3DVertex(centrum.X -length, centrum.Y + length, centrum.Z + length,
                                  0,0,0,
                                  color, 0, 0);
    vertices[5] = video::S3DVertex(centrum.X + length, centrum.Y + length, centrum.Z + length,
                                  0,0,0,
                                  color, 0, 0);
    vertices[6] = video::S3DVertex(centrum.X + length, centrum.Y + length, centrum.Z -length,
                                  0,0,0,
                                  color, 0, 0);
    vertices[7] = video::S3DVertex(centrum.X -length, centrum.Y + length, centrum.Z -length,
                                  0,0,0, color, 0, 0);

}

BlockModel::~BlockModel()
{
  //dtor
}

unsigned BlockModel::getBlockSize()
{
    return block_size;
}

core::vector3df BlockModel::getCentrum()
{
    return centrum;
}

video::S3DVertex *BlockModel::getVertices()
{
    return vertices;
}

/* Check if block should be destroyed
*/
bool BlockModel::checkDestroy()
{
    --durability;
    if(durability <= 0)
    {
      return true;
    }
    else
    {
      return false;
    }

}

const int BlockModel::getNumberOfVertices()
{
  return NUMBER_OF_VERTICES;
}
