/*
Author:
    Wardak Aleksandra

Main class of project.
*/


#include "BlockSceneNode.h"
#include "BlockModel.h"
#include "BlockFactory.h"
#include "GameEventReceiver.h"
#include "GameEventController.h"
#include "SceneDrawManager.h"
#include "Logger.h"
#include "GameParams.h"
#include "MapController.h"

#include <irrlicht.h>
#include <iostream>
using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif
void createBounding(ISceneManager*, ICameraSceneNode*, vector3df const centrum);

int main()
{
  const std::string TAG = "MAIN";
  Logger& log = Logger::getInstance();

  GameEventReceiver receiver;
  SceneDrawManager& drawManager = SceneDrawManager::getInstance();

    // create device
	IrrlichtDevice *device =
		createDevice( video::EDT_OPENGL, dimension2d<u32>(640, 480), 32,
			false, false, false, &receiver);

	if (!device)
		return 1;

	device->setWindowCaption(L"Minecraft");

	IVideoDriver* driver = device->getVideoDriver();
	ISceneManager* smgr = device->getSceneManager();
	IGUIEnvironment* guienv = device->getGUIEnvironment();

  drawManager.setSceneManager(smgr);

  MapController& map_controller = MapController::getInstance();
  map_controller.drawDefaultMap();

// create keyMap for camera
  SKeyMap keyMap[9];
  unsigned mapSize_ = 9;
  keyMap[0].Action = EKA_MOVE_FORWARD;
  keyMap[0].KeyCode = KEY_UP;
  keyMap[1].Action = EKA_MOVE_FORWARD;
  keyMap[1].KeyCode = KEY_KEY_W;

  keyMap[2].Action = EKA_MOVE_BACKWARD;
  keyMap[2].KeyCode = KEY_DOWN;
  keyMap[3].Action = EKA_MOVE_BACKWARD;
  keyMap[3].KeyCode = KEY_KEY_S;

  keyMap[4].Action = EKA_STRAFE_LEFT;
  keyMap[4].KeyCode = KEY_LEFT;
  keyMap[5].Action = EKA_STRAFE_LEFT;
  keyMap[5].KeyCode = KEY_KEY_A;

  keyMap[6].Action = EKA_STRAFE_RIGHT;
  keyMap[6].KeyCode = KEY_RIGHT;
  keyMap[7].Action = EKA_STRAFE_RIGHT;
  keyMap[7].KeyCode = KEY_KEY_D;

  keyMap[8].Action = EKA_JUMP_UP;
  keyMap[8].KeyCode = KEY_SPACE;

//smgr->addCameraSceneNodeFPS(parent, rotateSpeed, moveSpeed, ...);
  scene::ICameraSceneNode* camera = smgr->addCameraSceneNodeFPS(0, 75.0f, 0.05f, -1, keyMap, mapSize_, true, 0.5);
  camera->setPosition(core::vector3df(100,20,100));
  device->getCursorControl()->setVisible(true);

	int lastFPS = -1;

	while(device->run())
	{
    if (device->isWindowActive())
    {
      if (GameEventController::process(receiver, smgr, camera) == -1)
      {
        return 0;
      };

		driver->beginScene(true, true, SColor(255,100,101,140));

		smgr->drawAll();
		guienv->drawAll();
		driver->endScene();
    int fps = driver->getFPS();

      if (lastFPS != fps)
      {
        core::stringw str = L"Minecraft [";
        str += driver->getName();
        str += "] FPS:";
        str += fps;
        str += ", mouse position: ";
        str += receiver.GetMouseState().Position.X;
        str += ", ";
        str += receiver.GetMouseState().Position.Y;

        device->setWindowCaption(str.c_str());
        lastFPS = fps;
      }
		}
    else
    {
      device->yield();
    }
	}

	device->drop();

	return 0;
}
