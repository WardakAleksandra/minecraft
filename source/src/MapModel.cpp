#include "MapModel.h"
#include "GameParams.h"

MapModel::MapModel()
{
  //ctor
}

MapModel::~MapModel()
{
  //dtor
}

bool MapModel::createDefaultMap()
{
  int y = 0;
  for(int x=0; x<GameParams::MAP_SIZE; ++x)
  {
    for(int z=0; z<GameParams::MAP_SIZE; ++z)
    {
      game_map[std::make_tuple(x, y, z)] = Type::ROCK;
      game_map[std::make_tuple(x, y + 1, z)] = Type::GROUND;
    }
  }

  return true;
}

std::map<std::tuple<int,int,int>, Type> MapModel::getGameMap() const
{
  return game_map;
}

void MapModel::setGameMap(std::map<std::tuple<int,int,int>, Type> const game_map)
{
  this->game_map = game_map;
}
