#include "SceneDrawManager.h"
#include "BlockFactory.h"
#include "BlockSceneNode.h"
#include "MapController.h"
#include "Logger.h"
#include <stdexcept>
#include <tuple>
#include <algorithm>
#include <vector3d.h>
//#include <ISceneNode.h>
//#include <ITriangleSelector.h>

SceneDrawManager& SceneDrawManager::getInstance()
{
  static SceneDrawManager instance;
  return instance;
}

SceneDrawManager::SceneDrawManager() : Log(Logger::getInstance())
{

}

SceneDrawManager::~SceneDrawManager()
{
  //dtor
}

void SceneDrawManager::drawBlock(irr::core::vector3di const indexes, Type const type)
{
  if (!smgr)
  {
    throw std::runtime_error("Scene manager not initialized!");
  }
  /*
  if (!camera)
  {
    throw std::runtime_error("Camera not initialized!");
  }
  */
  irr::core::vector3df position = MapController::getInstance().indexesToCoords(indexes);

  int id_block = MapController::getInstance().createBlockId(indexes);
  Log.d(TAG, "ID_BLOCK: " + std::to_string(id_block));

  BlockModel* block_model= BlockFactory::createBlock(type, position);
  BlockSceneNode* block_scene_node= new BlockSceneNode(smgr->getRootSceneNode(), smgr,
                           id_block, block_model);
/*
 // irr::scene::ISceneNode* scene_node = smgr->addCubeSceneNode(GameParams::BLOCK_SIZE,  0, 1, position);
  irr::scene::ITriangleSelector* selector = 0;
  selector = smgr->createTriangleSelectorFromBoundingBox(block_scene_node);
  block_scene_node->setTriangleSelector(selector);

  if (selector)
  {
    scene::ISceneNodeAnimator* anim = smgr->createCollisionResponseAnimator(
        selector, camera, core::vector3df(30,50,30),
        core::vector3df(0,-10,0), core::vector3df(0,0,0));
    selector->drop(); // As soon as we're done with the selector, drop it.
    camera->addAnimator(anim);
    anim->drop();  // And likewise, drop the animator when we're done referring to it.
  }
*/
  block_scene_node->drop();
  block_scene_node = 0;
}

void SceneDrawManager::drawMap(MapModel const& map_model)
{
  if (!smgr)
  {
    throw std::runtime_error("Scene manager not initialized!");
  }
  smgr->clear();
  std::map<std::tuple<int,int,int>, Type> game_map = map_model.getGameMap();

  std::for_each(game_map.begin(), game_map.end(), [this](decltype(*begin(game_map)) &map_element){
               int x = std::get<0>(map_element.first);
               int y = std::get<1>(map_element.first);
               int z = std::get<2>(map_element.first);
              // log.d(TAG, std::to_string(x) +" " + std::to_string(y) +" "+ std::to_string(z));
               drawBlock(irr::core::vector3di(x, y, z), map_element.second );
                 });
}

void SceneDrawManager::setSceneManager( irr::scene::ISceneManager* const smgr)
{
  this->smgr = smgr;
}

void SceneDrawManager::setCamera( irr::scene::ICameraSceneNode* const camera)
{
  this->camera = camera;
}

void SceneDrawManager::removeBlock(irr::core::vector3di const indexes)
{
  if (!smgr)
  {
    throw std::runtime_error("Scene manager not initialized!");
  }
  int id = MapController::getInstance().createBlockId(indexes);
  Log.d(TAG, "REMOVE id: " + std::to_string(id));
  irr::scene::ISceneNode* block = smgr->getSceneNodeFromId(id);
  block->remove();
}
