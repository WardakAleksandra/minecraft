#include "BlockRock.h"

const video::SColor COLOR = video::SColor(255, 103,103,103);
const static unsigned DURABILITY_INIT = 3;

BlockRock::BlockRock(core::vector3df block_centrum)
: BlockModel(block_centrum, COLOR)
{
    durability = DURABILITY_INIT;
}

BlockRock::~BlockRock()
{
  //dtor
}
