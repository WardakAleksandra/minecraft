#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Hello
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <irrlicht.h>
#include "BlockSceneNode.h"

using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;


BOOST_AUTO_TEST_CASE( testDodaj )
{

    IrrlichtDevice *device =
		createDevice( video::EDT_OPENGL, dimension2d<u32>(640, 480), 32,
			false, false, false, 0);
    ISceneManager* smgr = device->getSceneManager();

    BlockSceneNode *myNode =
        new BlockSceneNode(smgr->getRootSceneNode(), smgr,
                           0, vector3df(0, 0, 0), 10);
      vector3d<f32> *edges;
      vector3df centrum = myNode->getBoundingBox().getCenter();
      BOOST_CHECK( centrum == vector3df(0, 0, 0) ); // Sprawdzanie warunku czy srodek szescianu jest w pkt 0 0 0

}


