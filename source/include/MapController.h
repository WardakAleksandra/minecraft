#ifndef MAPCONTROLLER_H
#define MAPCONTROLLER_H

#include "MapModel.h"
#include <vector3d.h>
#include "Logger.h"

enum class Type;

class MapController
{
  public:
    static MapController& getInstance();
    void drawDefaultMap();
    irr::core::vector3df indexesToCoords(irr::core::vector3di const) const;
    irr::core::vector3di coordsToIndexes(irr::core::vector3df const) const;
    bool isAreaAvailable(irr::core::vector3di const) const;
    void removeBlock(irr::core::vector3di const);
    void addBlock(irr::core::vector3di const, Type type);
    int createBlockId(irr::core::vector3di const);

  protected:
  private:
    MapController();
    MapController(const MapController&) = delete;
    MapController& operator=(const MapController&) = delete;

    Logger& Log = Logger::getInstance();
    const std::string TAG = "MapController";

    MapModel map_model;
};

#endif // MAPCONTROLLER_H
