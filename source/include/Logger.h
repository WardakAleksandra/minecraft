#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>

class Logger
{
  public:
    static Logger& getInstance();
    void d(std::string, std::string);
    void e(std::string, std::string);
    void i(std::string, std::string);
    void w(std::string, std::string);

  protected:
  private:
   Logger();
   Logger(const Logger&) = delete;
   Logger& operator=(const Logger&) = delete;

};

#endif // LOGGER_H
