#ifndef GAMEEVENTCONTROLLER_H
#define GAMEEVENTCONTROLLER_H

#include "ISceneManager.h"
#include "GameEventReceiver.h"
#include "MapController.h"

class GameEventController
{
  public:
    GameEventController();
    virtual ~GameEventController();

    static int process(GameEventReceiver&, irr::scene::ISceneManager*, irr::scene::ICameraSceneNode*);
  protected:
  private:

};

#endif // GAMEEVENTCONTROLLER_H
