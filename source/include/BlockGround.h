#ifndef BLOCKGROUND_H
#define BLOCKGROUND_H
#include "BlockModel.h"

class BlockGround: public BlockModel
{
    public:
        BlockGround(core::vector3df);
        virtual ~BlockGround();
    protected:
    private:
};

#endif // BLOCKGROUND_H
