#ifndef MAPMODEL_H
#define MAPMODEL_H

#include <map>
#include "BlockFactory.h"

class MapModel
{
  public:
    MapModel();
    virtual ~MapModel();

    bool createDefaultMap();
    std::map<std::tuple<int,int,int>, Type> getGameMap() const;
    void setGameMap(std::map<std::tuple<int,int,int>, Type> const);

  protected:
  private:
    std::map<std::tuple<int,int, int>, Type> game_map;
};

#endif // MAPMODEL_H
