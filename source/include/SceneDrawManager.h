#ifndef SCENEDRAWMANAGER_H
#define SCENEDRAWMANAGER_H

#include <vector3d.h>
#include <ISceneManager.h>
#include <string>

class MapModel;
class Logger;
enum class Type;

class SceneDrawManager
{
  public:
    static SceneDrawManager& getInstance();
    void drawBlock(irr::core::vector3di const, Type const);
    void removeBlock(irr::core::vector3di const);
    void setSceneManager( irr::scene::ISceneManager* const);
    void setCamera( irr::scene::ICameraSceneNode* const);
    void drawMap(MapModel const&);

  protected:
  private:
    SceneDrawManager();
    virtual ~SceneDrawManager();

    Logger& Log;
    const std::string TAG = "SceneDrawManager";

    irr::scene::ISceneManager* smgr;
    irr::scene::ICameraSceneNode* camera;
};

#endif // SCENEDRAWMANAGER_H
