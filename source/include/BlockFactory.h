#ifndef BLOCKFACTORY_H
#define BLOCKFACTORY_H

#include "BlockModel.h"

enum class Type {GROUND, ROCK};

class BlockFactory
{
    public:
        BlockFactory();
        virtual ~BlockFactory();
        static BlockModel* createBlock(const Type, core::vector3df);
    protected:
    private:
};

#endif // BLOCKFACTORY_H
