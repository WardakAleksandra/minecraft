#ifndef KEYBOARDEVENTRECEIVER_H
#define KEYBOARDEVENTRECEIVER_H

//#include <irrlicht.h>
#include <IEventReceiver.h>
#include <vector2d.h>

//using namespace irr;

class GameEventReceiver : public irr::IEventReceiver
{
  public:
    struct SMouseState
    {
        irr::core::vector2d<int> Position;
        bool LeftButtonDown;
        SMouseState() : LeftButtonDown(false) { }
    } MouseState;

    GameEventReceiver();
    virtual ~GameEventReceiver();
    virtual bool OnEvent(const irr::SEvent&s);
    virtual bool IsKeyDown(irr::EKEY_CODE keyCode) const;
    const SMouseState& GetMouseState(void) const;
    void clearMouseState(bool);
  protected:
  private:
        // Use this array to store the current state of each key
    bool keyIsDown[irr::KEY_KEY_CODES_COUNT];
};

#endif // KEYBOARDEVENTRECEIVER_H
