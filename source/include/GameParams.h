#ifndef GAMEPARAMS_H_INCLUDED
#define GAMEPARAMS_H_INCLUDED

//#include "BlockFactory.h"
#include <SKeyMap.h>

enum class Type;

class GameParams
{
  public:
    static Type current_type;
  //  static irr::SKeyMap& createKeyMap(irr::SKeyMap&);
   // const static s32 KEY_MAP_SIZE = 9;
    const static int MAP_SIZE = 50;
    const static int BLOCK_SIZE = 10;
};

#endif // GAMEPARAMS_H_INCLUDED
