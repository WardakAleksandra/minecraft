#ifndef BLOCKROCK_H
#define BLOCKROCK_H
#include "BlockModel.h"

class BlockRock : public BlockModel
{
    public:
        BlockRock(core::vector3df);
        virtual ~BlockRock();
    protected:
    private:
};

#endif // BLOCKROCK_H
