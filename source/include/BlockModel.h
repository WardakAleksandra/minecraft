#ifndef BLOCKMODEL_H
#define BLOCKMODEL_H

#include <irrlicht.h>
#include "GameParams.h"

using namespace irr;

class BlockModel
{

    public:

      BlockModel(core::vector3df, const video::SColor&, unsigned block_size = GameParams::BLOCK_SIZE);
      virtual ~BlockModel();
      enum class BlockType{GROUND, ROCK};
      unsigned getBlockSize();
      core::vector3df getCentrum();
      video::S3DVertex *getVertices();
      const int getNumberOfVertices();


    protected:
      const static int NUMBER_OF_VERTICES = 8;

      unsigned block_size;
      core::vector3df centrum;
      unsigned durability;
      bool checkDestroy();
      video::S3DVertex vertices[NUMBER_OF_VERTICES];
    private:

};

#endif // BLOCKMODEL_H
