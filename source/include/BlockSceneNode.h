/*
Author:
    Wardak Aleksandra

Class BlockSceneNode describes signle block used to create game world.
*/

#ifndef BLOCKSCENENODE_H
#define BLOCKSCENENODE_H

#include <irrlicht.h>
#include <BlockModel.h>

using namespace irr;

class BlockSceneNode : public scene::ISceneNode
{
    const static int NUMBER_OF_SIDES = 6;

    core::aabbox3d<f32> box;
    video::SMaterial material;

  public:
    BlockSceneNode(scene::ISceneNode*, scene::ISceneManager*, s32, BlockModel*);
    virtual ~BlockSceneNode();
    virtual void OnRegisterSceneNode();
    virtual void render();
    virtual const core::aabbox3d<f32>& getBoundingBox() const;
    virtual u32 getMaterialCount() const;
    virtual video::SMaterial& getMaterial(u32 i);

  protected:
  private:
    video::S3DVertex* vertices;
    unsigned number_of_vertices;
};
int dodaj( int i, int j );
#endif // BLOCKSCENENODE_H


